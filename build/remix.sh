#!/bin/sh

# Add universe and multiverse.
add-apt-repository -y --no-update universe
add-apt-repository -y --no-update multiverse

# Add PopOS key and repos.

sudo apt-key adv --keyserver keyserver.ubuntu.com  --recv-keys 204DD8AEC33A7AFF

cat > /etc/apt/sources.list.d/pop-os.list <<EOF
deb https://apt.pop-os.org/release/ impish main
deb https://apt.pop-os.org/proprietary impish main
EOF

apt-get update -y

# Install software
apt-get purge -y ubuntu-session snapd && apt install -y wget curl jq unzip sed gnome-tweaks \
 gnome-shell-extension-prefs build-essential git node-typescript flatpak gnome-software gnome-software-plugin-flatpak
flatpak remote-add --system --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
apt-get install -y pop-theme pop-shell pop-session pop-default-settings system76-power system76-acpi-dkms \
  system76-firmware system76-io-dkms  plymouth-theme-pop-logo system76-io-dkms && apt-get purge -y pop-cosmic

# Installs gnome extensions.
apt install gnome-shell-extension-gsconnect gnome-shell-extension-system76-power  \
 gnome-shell-extension-dashtodock gnome-shell-extension-no-annoyance
bash ./install-gnome-extensions.sh --enable 517 779 448  4228
# Extensions are as follows: caffeine clipboard-indicator remove-rounded-corners sound-output-device-chooser
